<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package VW Fitness Gym
 */

get_header(); ?>

<main id="maincontent" role="main" class="content-vw">
	<div class="container">
		<div class="page-content">
	    	<h1><?php echo esc_html(get_theme_mod('vw_fitness_gym_404_page_title',__('ERROR 404! Page Not Found','vw-fitness-gym')));?></h1>
			<p class="text-404"><?php echo esc_html(get_theme_mod('vw_fitness_gym_404_page_content',__('This page does not exist!','vw-fitness-gym')));?></p>
			<div class="more-btn">
		        <a href="<?php echo esc_url(get_permalink()); ?>"><?php echo esc_html(get_theme_mod('vw_fitness_gym_404_page_button_text',__('GO BACK','vw-fitness-gym')));?><span class="screen-reader-text"><?php esc_html_e( 'GO BACK','vw-fitness-gym' );?></span></a>
		    </div>
		</div>
		<div class="clearfix"></div>
	</div>
</main>

<?php get_footer(); ?>